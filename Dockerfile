FROM node:alpine

RUN npm install -g mockserver

EXPOSE 8080

CMD mockserver -p 8080 -m /mocks
